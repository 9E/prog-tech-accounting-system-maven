package accounting.webcontrollers;

import accounting.mvcjavafx.gson.*;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.Company;
import accounting.mvcjavafx.model.Constants;
import accounting.mvcjavafx.model.Person;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Properties;

@Controller
public class CategoryWebController {

    EntityManagerFactory entityManagerFactory;
    CategoryHibernateControl categoryHibernateControl;
    Gson gson;

    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Expense.class, new ExpenseJsonSerializer());
        gsonBuilder.registerTypeAdapter(Category.class, new CategoryJsonSerializer());
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyJsonSerializer());
        gsonBuilder.registerTypeAdapter(Person.class, new PersonJsonSerializer());

        this.gson = gsonBuilder.create();
        this.entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
        this.categoryHibernateControl = new CategoryHibernateControl(entityManagerFactory);
    }

    @RequestMapping(value = "category/getAllSupervisedCategoriesByUser", method = RequestMethod.GET)
    @ResponseBody
    public String getAllSupervisedCategoriesByUser(@RequestParam Long userId){
        List<Category> categories = categoryHibernateControl.getAllSupervisedCategories(userId);
        return gson.toJson(categories);
    }

    @RequestMapping(value = "category/getAllRootCategories", method = RequestMethod.GET)
    @ResponseBody
    public String getAllRootCategories(){
        List<Category> categories = categoryHibernateControl.getAllRootCategories();
        return gson.toJson(categories);
    }

    @RequestMapping(value = "category/getCategoryById", method = RequestMethod.GET)
    @ResponseBody
    public String getCategoryById(@RequestParam("id") String id){
        Category category = categoryHibernateControl.getCategoryById(Long.parseLong(id));
        return gson.toJson(category);
    }

    @RequestMapping(value = "category/updateCategory", method = RequestMethod.PUT)
    @ResponseBody
    public String updateCategory(@RequestBody String request){
        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String id = data.getProperty("id");
        String name = data.getProperty("newName");
        Category category = categoryHibernateControl.getCategoryById(Long.parseLong(id));
        category.setName(name);
        categoryHibernateControl.edit(category);
        return "Category updated";
    }
}
