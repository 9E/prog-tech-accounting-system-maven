package accounting.webcontrollers;

import accounting.mvcjavafx.gson.*;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.*;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Properties;

@Controller
public class UserWebController {


    EntityManagerFactory entityManagerFactory;
    UserHibernateControl userHibernateControl;
    Gson gson;

    {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Income.class, new IncomeJsonSerializer());
        gsonBuilder.registerTypeAdapter(Expense.class, new ExpenseJsonSerializer());
        gsonBuilder.registerTypeAdapter(Category.class, new CategoryJsonSerializer());
        gsonBuilder.registerTypeAdapter(Company.class, new CompanyJsonSerializer());
        gsonBuilder.registerTypeAdapter(Person.class, new PersonJsonSerializer());

        this.gson = gsonBuilder.create();
        this.entityManagerFactory = Persistence.createEntityManagerFactory(Constants.SCHEMA_NAME);
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
    }

    @RequestMapping(value = "user/getUserById", method = RequestMethod.GET)
    @ResponseBody
    public String getUserById(@RequestParam Long id){
        User user = userHibernateControl.getUserById(id);
        return gson.toJson(user);
    }

    @RequestMapping(value = "user/getUserByUsername", method = RequestMethod.GET)
    @ResponseBody
    public String getUserByUsername(@RequestParam String username){
        User user = userHibernateControl.getUserByUsername(username);
        return gson.toJson(user);
    }

    @RequestMapping(value = "user/isLoginInfoCorrect", method = RequestMethod.POST)
    @ResponseBody
    public String isLoginInfoCorrect(@RequestBody String request){
        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String username = data.getProperty("username");
        String password = data.getProperty("password");
        if(userHibernateControl.isLoginInfoCorrect(username, password)) {
            return userHibernateControl.getUserByUsername(username).getId().toString();
        }
        return "Login info incorrect";
    }

    @RequestMapping(value = "user/updateUser", method = RequestMethod.PUT)
    @ResponseBody
    public String updateUser(@RequestBody String request){
        Gson parser = new Gson();
        Properties data = parser.fromJson(request, Properties.class);
        String id = data.getProperty("id");
        String username = data.getProperty("username");
        String password = data.getProperty("password");
        String address = data.getProperty("address");
        String email = data.getProperty("email");
        String phoneNumber = data.getProperty("phoneNumber");
        User user = userHibernateControl.getUserById(Long.parseLong(id));
        user.setUsername(username);
        user.setPassword(password);
        user.getContactInfo().setAddress(address);
        user.getContactInfo().setEmail(email);
        user.getContactInfo().setPhoneNumber(phoneNumber);
        userHibernateControl.edit(user);
        return "User updated";
    }
}
