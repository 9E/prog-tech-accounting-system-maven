package accounting.commandlineui;

import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.Company;
import accounting.mvcjavafx.model.Person;

import java.util.Scanner;

public class Registration{
    private CommandLineMenu commandLineMenu;
    private AccountingSystem accountingSystem;
    private Scanner scanner = new Scanner(System.in);

    public Registration(CommandLineMenu commandLineMenu, AccountingSystem accountingSystem) {
        this.commandLineMenu = commandLineMenu;
        this.accountingSystem = accountingSystem;
    }

    public void createNewAccountPrompt() {
        boolean exitState = false;

        while(!exitState) {
            System.out.println("Choose account type:");
            System.out.println("1. Person");
            System.out.println("2. Company");
            String input = scanner.nextLine().trim();


            switch (input) {
                case "1":
                    personRegistrationPrompt();
                    break;
                case "2":
                    companyRegistrationPrompt();
                    break;
                default:
                    System.out.println("Wrong choice, please type again.");
            }

            exitState = commandLineMenu.returnToWelcomeMenu();
        }
    }

    public void personRegistrationPrompt() {
        boolean exitState = false;

        while(!exitState) {
            System.out.println("Please enter your username:");
            String username = scanner.nextLine();
            System.out.println("Please enter your password:");
            String password = scanner.nextLine();
            System.out.println("Please enter your first name:");
            String firstName = scanner.nextLine();
            System.out.println("Please enter your last name:");
            String lastName = scanner.nextLine();

            if (accountingSystem.isUsernameTaken(username)) {
                System.out.println("Such username is already taken");
            } else {
                User person = new Person(username, password, null, firstName, lastName);
                accountingSystem.getSystemUsers().add(person);
                System.out.println("You have succesfuly created a person account");
                commandLineMenu.welcomePrompt();
            }

            exitState = commandLineMenu.returnToWelcomeMenu();
        }
    }

    public void companyRegistrationPrompt() {
        boolean exitState = false;

        while(!exitState) {
            System.out.println("Please enter your username:");
            String username = scanner.nextLine();
            System.out.println("Please enter your password:");
            String password = scanner.nextLine();
            System.out.println("Please enter your company name:");
            String companyName = scanner.nextLine();

            if (accountingSystem.isUsernameTaken(username)) {
                System.out.println("Such username is already taken");
            } else {
                User company = new Company(username, password, null, companyName);
                accountingSystem.getSystemUsers().add(company);
                System.out.println("You have succesfuly created a company account");
                commandLineMenu.welcomePrompt();
            }

            exitState = commandLineMenu.returnToWelcomeMenu();
        }
    }
}
