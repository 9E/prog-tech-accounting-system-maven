package accounting.commandlineui;

import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.accsys.AccountingSystemMaintainer;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;

import java.util.List;
import java.util.Scanner;

public class MainMenu {
    private CommandLineMenu commandLineMenu;
    private AccountingSystem accountingSystem;
    private Scanner scanner = new Scanner(System.in);
    private User currentUser;

    public MainMenu(CommandLineMenu commandLineMenu, AccountingSystem accountingSystem) {
        this.commandLineMenu = commandLineMenu;
        this.accountingSystem = accountingSystem;
        this.currentUser = null;
    }

    public void mainMenuPrompt() {

        System.out.println("Main menu.");
        System.out.println("What would you like to do?");
        System.out.println("1. View supervised categories");
        System.out.println("2. Create new category");
        System.out.println("3. Delete your account (without warning prompt. System will turn off )");
        System.out.println("4. Exit");
        String input = scanner.nextLine().trim().toLowerCase();

        switch (input) {
            case "1":
                viewSupervisedCategories();
                break;
            case "2":
                createNewCategory();
                break;
            case "3":
                this.accountingSystem.getSystemUsers().remove(currentUser);
                System.out.println("Farewell.");
                AccountingSystemMaintainer.saveAccountingSystem(this.accountingSystem);
                System.exit(0);
                break;
            case "4":
                if (commandLineMenu.wantToExit()) {
                    AccountingSystemMaintainer.saveAccountingSystem(this.accountingSystem);
                    System.out.println("Bye!");
                    System.exit(0);
                }
                break;
            default:
                System.out.println("Wrong choice, please choose again.");
        }

        mainMenuPrompt();
    }

    public void createNewCategory() { // Menu "2"
        System.out.println("Enter category name");
        String categoryName = scanner.nextLine();
        currentUser.addParentCategory(categoryName);
    }

    public void viewSupervisedCategories() { // Menu "1"
        List<Category> currentCategoryView = currentUser.getSupervisedCategories();


        boolean exitState = false;

        while (!exitState) {
            if (currentCategoryView == currentUser.getSupervisedCategories() &&
                    currentCategoryView.size() == 0) {
                System.out.println("You don't have any supervised categories");
                exitState = true;
            } else {

                displayCategories(currentCategoryView);
                System.out.println("Choose a category (enter id) (or enter any integer to exit this menu. Note, don't enter anything but integers)");

                int inputId = Integer.parseInt(this.scanner.nextLine().trim());

                Category chosenCategory = chooseCategory(currentCategoryView, inputId);
                if (chosenCategory != null) {
                    currentCategoryView = chosenCategory.getSubcategories();
                } else {
                    System.out.println("Incorrect choice, please try again.");
                }

                if (exitState = commandLineMenu.wantToExit()) {
                    break;
                }

                if (chosenCategory != null) {
                    categoryViewChoices(chosenCategory);
                }
            }
        }

    }

    void categoryViewChoices(Category category) {
        System.out.println("What would you like to do with the chosen category? " + category);
        System.out.println("1. Descend to to lower categories");
        System.out.println("2. Create subcategory");
        System.out.println("3. Assign category to another user");
        System.out.println("4. Add income to category");
        System.out.println("5. Add expense to category");
        System.out.println("6. Get balance of category without subcategories");
        System.out.println("7. Delete this category");
        System.out.println("8. Update category name");
        System.out.println("9. Get balance of current category and of its all subcategories");
        System.out.println("10. Exit this menu");
        String choice = scanner.nextLine().trim();

        switch (choice) {
            case "1":
                if (category.getSubcategories().size() == 0) {
                    System.out.println("The category doesn't have any subcategories. Choose another action.");
                    categoryViewChoices(category);
                }
                break;
            case "2":
                createNewSubcategory(category);
                categoryViewChoices(category);
                break;
            case "3":
                System.out.println("Enter user's username you want to assign the category to (you can assign it to yourself too)");
                String username = scanner.nextLine();
                User assignee = this.accountingSystem.getUserByUsername(username);
                if (assignee != null) {
                    currentUser.assignCategoryTo(assignee, category);
                    System.out.printf("Assigned successfully");
                } else {
                    System.out.println("Such user doesn't exist");
                }
                categoryViewChoices(category);
                break;
            case "4":
                addIncomeToCategory(category);
                categoryViewChoices(category);
                break;
            case "5":
                addExpenseToCategory(category);
                categoryViewChoices(category);
                break;
            case "6":
                System.out.println("Category's " + category + " balance is = " + category.getBalance());
                categoryViewChoices(category);
                break;
            case "7":
                if (category.getParentCategory() != null) {
                    Category parentCategory = category.getParentCategory();
                    parentCategory.getSubcategories().remove(category);
                } else {
                    currentUser.getSupervisedCategories().remove(category);
                }
                System.out.println("done");
                return;
            case "8":
                System.out.println("Enter new category name:");
                String newCategoryName = scanner.nextLine();
                category.setName(newCategoryName);
                System.out.println("done");
                categoryViewChoices(category);
                return;
            case "9":
                System.out.println(category.getBalanceWithSubcategories());
                categoryViewChoices(category);
                break;
            case "10":
                return;
            default:
                System.out.println("Wrong choice, please choose again");
                categoryViewChoices(category);
        }
    }

    private void addExpenseToCategory(Category category) {
        System.out.println("Enter expense money amount (enter positive value for correct calculations)");
        double moneyAmount = Double.parseDouble(scanner.nextLine().trim());
        System.out.println("Enter expense name");
        String expenseName = scanner.nextLine();
        System.out.println("Enter receipt number (can include any symbols)");
        String expenseReceiptNumber = scanner.nextLine();
        category.addExpense(new Expense(expenseName, currentUser, moneyAmount, expenseReceiptNumber));
    }

    private void addIncomeToCategory(Category category) {
        System.out.println("Enter income money amount");
        double moneyAmount = Double.parseDouble(scanner.nextLine().trim());
        System.out.println("Enter income name");
        String incomeName = scanner.nextLine();
        category.addIncome(new Income(incomeName, currentUser, moneyAmount));
    }

    public void createNewSubcategory(Category parentCategory) { // Menu "2"
        System.out.println("Enter subcategory name");
        String categoryName = scanner.nextLine();

        Category subcategory = new Category(currentUser, parentCategory, categoryName);
        parentCategory.addSubcategory(subcategory);
        System.out.println("Subcategory was added.");
    }

    private Category chooseCategory(List<Category> categories, int categoryId) {
        for (Category category : categories) {
            if (category.getId() == categoryId) {
                return category;
            }
        }
        return null;
    }


    private void displayCategories(User user) {
        for (Category category : user.getSupervisedCategories()) {
            System.out.println(category);
        }
    }

    private void displayCategories(Category parentCategory) {
        for (Category category : parentCategory.getSubcategories()) {
            System.out.println(category);
        }
    }

    private void displayCategories(List<Category> categories) {
        for (Category category : categories) {
            System.out.println(category);
        }
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
