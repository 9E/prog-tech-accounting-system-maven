package accounting.mvcjavafx.model.accsys;

import accounting.mvcjavafx.model.User;

import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class VanillaAccountingSystem extends AccountingSystem {

    public VanillaAccountingSystem(String name, String version) {
        super(name, version);
    }

    public VanillaAccountingSystem(String name, String version, LocalDate creationDate) {
        super(name, version, creationDate);
    }

    public VanillaAccountingSystem() {
    }

    @Override
    public User getUserByUsername(String username) {
        for (User user : super.getSystemUsers()) {
            if (username.equals(user.getUsername())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public <T> User getUserById(T id) {
        for (User user : super.getSystemUsers()) {
            if (Objects.equals(id, user.getId())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public boolean isUsernameTaken(String username) {
        for (User systemUser : super.getSystemUsers()) {
            if (username.equals(systemUser.getUsername())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isLoginInfoCorrect(String username, String password) {
        for (User systemUser : super.getSystemUsers()) {
            if (doCredentialsMatchWithUserAccount(username, password, systemUser)) {
                return true;
            }
        }
        return false;
    }

    private boolean doCredentialsMatchWithUserAccount(String username, String password, User user) {
        return username.equals(user.getUsername()) &&
                password.equals(user.getPassword());
    }
}
