package accounting.mvcjavafx.model;

import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import accounting.mvcjavafx.model.moneyflow.MoneyFlow;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private User creator;
    private String name;
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Income> incomes;
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Expense> expenses;
    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> supervisors;
    private LocalDate creationDate;

    @OneToMany(mappedBy = "parentCategory", cascade = CascadeType.ALL, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Category> subcategories;
    @ManyToOne
    private Category parentCategory;

    @ManyToOne
    private AccountingSystem accountingSystem;

    public Category(User creator, Category parentCategory, String name) {
        this.creator = creator;
        this.parentCategory = parentCategory;
        this.name = name;
        this.incomes = new ArrayList<>();
        this.expenses = new ArrayList<>();
        this.supervisors = new ArrayList<>();
        this.subcategories = new ArrayList<>();
        this.creationDate = LocalDate.now();
        addSupervisor(creator);
    }

    public Category(String name) {
        this.creator = null;
        this.parentCategory = null;
        this.name = name;
        this.incomes = new ArrayList<>();
        this.expenses = new ArrayList<>();
        this.supervisors = new ArrayList<>();
        this.subcategories = new ArrayList<>();
        this.creationDate = LocalDate.now();
    }

    public Category() {
        this.incomes = new ArrayList<>();
        this.expenses = new ArrayList<>();
        this.supervisors = new ArrayList<>();
        this.subcategories = new ArrayList<>();
    }

    public void deleteThisCategory() {
        if (this.hasParentCategory()) {
            this.getParentCategory().getSubcategories().remove(this);
        } else {
            this.getCreator().getSupervisedCategories().remove(this);
        }
    }

    public boolean hasCreator() {
        return creator != null;
    }

    public boolean hasParentCategory() {
        return parentCategory != null;
    }

    public double getBalanceWithSubcategories() {
        return getBalance() + getChildrenBalanceRecursively();
    }

    public double getSumOfIncomesWithSubcategories() {
        return getSumOfIncomes() + getSumOfIncomesRecursively();
    }

    public double getSumOfExpensesWithSubcategories() {
        return getSumOfExpenses() + getSumOfExpensesRecursively();
    }

    public double getChildrenBalanceRecursively() {
        return getSumOfIncomesRecursively() - getSumOfExpensesRecursively();
    }

    public double getSumOfIncomesRecursively() {
        double sum = 0;
        for (Category subcategory : this.subcategories) {
            sum += subcategory.getSumOfIncomes();
            sum += subcategory.getSumOfIncomesRecursively();
        }
        return sum;
    }

    public double getSumOfExpensesRecursively() {

        double sum = 0;
        for (Category subcategory : this.subcategories) {
            sum += subcategory.getSumOfExpenses();
            sum += subcategory.getSumOfExpensesRecursively();
        }
        return sum;
    }

    public void assignSupervisorToSubcategoriesRecursively(User supervisor) {
        this.supervisors.add(supervisor);
        for (Category subcategory : this.subcategories) {
            subcategory.assignSupervisorToSubcategoriesRecursively(supervisor);
        }
    }

    public boolean isSupervisedBy(Long userId) {
        for (User supervisor: this.supervisors) {
            if (Objects.equals(userId, supervisor.getId())) {
                return true;
            }
        }
        return false;
    }

    public double getBalance() {
        return getSumOfIncomes() - getSumOfExpenses();
    }

    public double getSumOfIncomes() {
        return getSumOfMoneyFlows(incomes);
    }

    public double getSumOfExpenses() {
        return getSumOfMoneyFlows(expenses);
    }

    private double getSumOfMoneyFlows(List<? extends MoneyFlow> moneyFlows) {
        double totalSum = 0;
        for (MoneyFlow moneyFlow : moneyFlows) {
            totalSum += moneyFlow.getAmount();
        }
        return totalSum;
    }

    public void addSupervisor(User user) {
        this.supervisors.add(user);
    }

    public void addIncome(Income income) {
        this.incomes.add(income);
    }

    public void addExpense(Expense expense) {
        this.expenses.add(expense);
    }

    public void addSubcategory(Category subcategory) {
        subcategory.setParentCategory(this);
        subcategory.setAccountingSystem(this.accountingSystem);
        this.subcategories.add(subcategory);
    }

    @Override
    public String toString() {
        String returnString = "Category{" +
                "id=" + id +
                ", name='" + name + '\'' +
                "}  (has " + this.subcategories.size() + " subcategor";
        if (this.subcategories.size() == 1) {
            returnString += "y)";
        } else {
            returnString += "ies)";
        }
        return returnString;
    }

    public List<Category> getSubcategories() {
        return subcategories;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(List<Income> incomes) {
        this.incomes = incomes;
    }

    public List<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(List<Expense> expenses) {
        this.expenses = expenses;
    }

    public List<User> getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(List<User> supervisors) {
        this.supervisors = supervisors;
    }

    public void setSubcategories(List<Category> subcategories) {
        this.subcategories = subcategories;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public AccountingSystem getAccountingSystem() {
        return accountingSystem;
    }

    public void setAccountingSystem(AccountingSystem accountingSystem) {
        this.accountingSystem = accountingSystem;
    }
}
