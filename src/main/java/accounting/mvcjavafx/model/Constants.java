package accounting.mvcjavafx.model;

public class Constants {
    public static final String SCHEMA_NAME = "AccountingSchema";
    public static final Long DEFAULT_ACC_SYS_ID = 1L;
}
