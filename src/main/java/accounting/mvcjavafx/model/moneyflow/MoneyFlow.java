package accounting.mvcjavafx.model.moneyflow;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@MappedSuperclass
public abstract class MoneyFlow implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private double amount;
    @ManyToOne
    private User creator;
    private LocalDate creationDate;

    @ManyToOne
    private Category category;

    public MoneyFlow(String name, User creator, double amount) {
        this.name = name;
        this.creator = creator;
        this.amount = amount;
        this.creationDate = LocalDate.now();
    }

    public MoneyFlow(String name, double amount) {
        this.name = name;
        this.amount = amount;
        this.creationDate = LocalDate.now();
    }

    public MoneyFlow(String name, User creator, double amount, Category category) {
        this.name = name;
        this.creator = creator;
        this.amount = amount;
        this.creationDate = LocalDate.now();
        this.category = category;
    }

    public MoneyFlow() {
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        String creatorUsername = getCreator() == null ? "" : getCreator().getUsername();
        return "MoneyFlow{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                ", creator=" + creatorUsername +
                ", creationDate=" + creationDate +
                ", category=" + category +
                '}';
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }
}
