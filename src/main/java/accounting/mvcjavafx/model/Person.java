package accounting.mvcjavafx.model;

import accounting.mvcjavafx.model.accsys.AccountingSystem;

import javax.persistence.Entity;

@Entity
public class Person extends User {
    private String firstName;
    private String lastName;

    public Person(String username, String password, ContactInfo contactInfo,
                  String firstName, String lastName, AccountingSystem accountingSystem) {
        super(username, password, contactInfo, accountingSystem);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(String username, String password, ContactInfo contactInfo,
                  String firstName, String lastName) {
        super(username, password, contactInfo);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
