package accounting.mvcjavafx.controller;

import accounting.mvcjavafx.hibernate.AccountingSystemHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.*;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;

public class SignUpWindow{
    public TextField usernameField;
    public PasswordField passwordField;
    public TextField phoneNumberField;
    public TextField emailField;
    public TextField addressField;
    public TextField firstNameField;
    public TextField lastNameField;
    public TextField companyNameField;

    private EntityManagerFactory entityManagerFactory;
    private UserHibernateControl userHibernateControl;
    private AccountingSystemHibernateControl accountingSystemHibernateControl;

    public void performGoBack(ActionEvent actionEvent) {
        goToLoginWindow(actionEvent);
    }

    private void goToLoginWindow(ActionEvent actionEvent) {
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource(("../view/LoginWindow.fxml")));
        } catch (IOException e) {
            new Alert(Alert.AlertType.ERROR, "Failed to return to Log In window").show();
            e.printStackTrace();
        }
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        stage.setScene(new Scene(root));
    }

    public void performSignUpAsAPerson(ActionEvent actionEvent) {
        String username = usernameField.getText();

        if (usernameField.getText().isEmpty()) {
            new Alert(Alert.AlertType.ERROR, "Username can't be empty").show();
            return;
        }

        if (userHibernateControl.isUsernameTaken(username)) {
            new Alert(Alert.AlertType.ERROR, "The username is already taken").show();
            return;
        }

        if (passwordField.getText().isEmpty()) {
            new Alert(Alert.AlertType.ERROR, "Password can't be empty").show();
            return;
        }

        String password = passwordField.getText();
        String phoneNumber = phoneNumberField.getText();
        String email = emailField.getText();
        String address = addressField.getText();
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();

        ContactInfo contactInfo = new ContactInfo(phoneNumber, email, address);
        AccountingSystem accountingSystem = accountingSystemHibernateControl.getAccountingSystemById(Constants.DEFAULT_ACC_SYS_ID);
        User person = new Person(username, password, contactInfo, firstName, lastName, accountingSystem);

        userHibernateControl.create(person);

        new Alert(Alert.AlertType.INFORMATION, "You have successfully created an account").show();
        goToLoginWindow(actionEvent);
    }

    public void performSignUpAsACompany(ActionEvent actionEvent) {
        String username = usernameField.getText();

        if (!userHibernateControl.isUsernameTaken(username)) {
            String password = passwordField.getText();
            String phoneNumber = phoneNumberField.getText();
            String email = emailField.getText();
            String address = addressField.getText();
            String companyName = companyNameField.getText();

            ContactInfo contactInfo = new ContactInfo(phoneNumber, email, address);
            AccountingSystem accountingSystem = accountingSystemHibernateControl.getAccountingSystemById(Constants.DEFAULT_ACC_SYS_ID);
            User company = new Company(username, password, contactInfo, companyName, accountingSystem);


            userHibernateControl.create(company);

            new Alert(Alert.AlertType.INFORMATION, "You have successfully created an account").show();
            goToLoginWindow(actionEvent);
        } else {
            String errorMessage = "The username is already taken";
            Alert alert = new Alert(Alert.AlertType.ERROR, errorMessage);
            alert.show();
        }
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
        this.accountingSystemHibernateControl = new AccountingSystemHibernateControl(entityManagerFactory);
    }
}
