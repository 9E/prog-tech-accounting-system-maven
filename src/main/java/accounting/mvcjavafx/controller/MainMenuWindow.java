package accounting.mvcjavafx.controller;

import accounting.mvcjavafx.controller.forms.AccountDataForm;
import accounting.mvcjavafx.controller.forms.CategoryFormWindow;
import accounting.mvcjavafx.controller.forms.ExpenseForm;
import accounting.mvcjavafx.controller.forms.IncomeForm;
import accounting.mvcjavafx.hibernate.CategoryHibernateControl;
import accounting.mvcjavafx.hibernate.ExpenseHibernateControl;
import accounting.mvcjavafx.hibernate.IncomeHibernateControl;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import accounting.mvcjavafx.model.accsys.AccountingSystemMaintainer;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import javafx.beans.property.ObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static accounting.mvcjavafx.controller.ControllerUtil.closeActionEventsStage;

public class MainMenuWindow {
    public TreeView<Category> categoriesTreeView;
    public Tab categoriesTab;
    public TabPane tabPane;
    public Tab incomesTab;
    public Tab expensesTab;

    private AccountingSystem accountingSystem;
    private User loggedInUser;

    private EntityManagerFactory entityManagerFactory;
    private UserHibernateControl userHibernateControl;
    private CategoryHibernateControl categoryHibernateControl;

    public void customInitialize(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
        this.categoryHibernateControl = new CategoryHibernateControl(entityManagerFactory);
        refillCategoriesTreeView();
    }

    public void refillCategoriesTreeView() {
        TreeItem<Category> rootItem = new TreeItem<>(new Category("Dad"));
        categoriesTreeView.setRoot(rootItem);
        categoriesTreeView.setShowRoot(false);

        categoryHibernateControl.getAllRootCategories().forEach(category -> addTreeItem(category, rootItem));
    }

    private void addTreeItem(Category category, TreeItem<Category> parentItem) {
        TreeItem<Category> categoryItem = new TreeItem<>(category);
        parentItem.getChildren().add(categoryItem);
        category.getSubcategories().forEach(cat -> addTreeItem(cat, categoryItem));
    }

    public void performAddParentCategory(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/CategoryFormWindow.fxml")));
        Parent root = loader.load();

        CategoryFormWindow createCategoryWindow = loader.getController();
        createCategoryWindow.customInitialize(entityManagerFactory, loggedInUser, categoriesTreeView.getRoot(), FormType.CREATE);

        ControllerUtil.showNewStage(root);
    }

    public void performAddSubcategory(ActionEvent actionEvent) throws IOException {
        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedCategoryItem.getValue();

        if (!categorySupervisedByLoggedInUser(selectedCategory.getId())) {
            new Alert(Alert.AlertType.ERROR, "You can't perform the action because you aren't supervising the category").show();
            return;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/CategoryFormWindow.fxml")));
        Parent root = loader.load();

        CategoryFormWindow createCategoryWindow = loader.getController();
        createCategoryWindow.customInitialize(entityManagerFactory, loggedInUser, selectedCategoryItem, FormType.CREATE);

        ControllerUtil.showNewStage(root);
    }

    public void performViewCategoryInfo(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/CategoryInfoWindow.fxml")));
        Parent root = loader.load();

        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();

        if (selectedCategoryItem != null) {
            CategoryInfoWindow categoryInfoWindow = loader.getController();
            categoryInfoWindow.customInitialize(selectedCategoryItem);

            ControllerUtil.showNewStage(root);
        }
    }

    public void performEditCategory(ActionEvent actionEvent) throws IOException {
        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedCategoryItem.getValue();

        if (!categorySupervisedByLoggedInUser(selectedCategory.getId())) {
            new Alert(Alert.AlertType.ERROR, "You can't perform the action because you aren't supervising the category").show();
            return;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/CategoryFormWindow.fxml")));
        Parent root = loader.load();

        CategoryFormWindow createCategoryWindow = loader.getController();
        createCategoryWindow.customInitialize(entityManagerFactory, loggedInUser, selectedCategoryItem, FormType.EDIT);
        ControllerUtil.showNewStage(root);
    }

    public void performDeleteCategory(ActionEvent actionEvent) {
        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedCategoryItem.getValue();

        if (!categorySupervisedByLoggedInUser(selectedCategory.getId())) {
            new Alert(Alert.AlertType.ERROR, "You can't perform the action because you aren't supervising the category").show();
            return;
        }

        selectedCategory = selectedCategoryItem.getValue();
        categoryHibernateControl.remove(selectedCategory.getId());
        selectedCategoryItem.getParent().getChildren().remove(selectedCategoryItem);
    }

    public void performAssignCategoryToAnotherUser(ActionEvent actionEvent) throws IOException {
        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedCategoryItem.getValue();

        if (!categorySupervisedByLoggedInUser(selectedCategory.getId())) {
            new Alert(Alert.AlertType.ERROR, "You can't perform the action because you aren't supervising the category").show();
            return;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/AssignCategoryToUserWindow.fxml")));
        Parent root = loader.load();

        AssignCategoryToUserWindow assignCategoryToUserWindow = loader.getController();
        assignCategoryToUserWindow.customInitialize(entityManagerFactory, loggedInUser, selectedCategoryItem);
        ControllerUtil.showNewStage(root);
    }


    public ListView<Income> incomesListView;
    public Label incomesCategoryLabel;
    public Category lastIncomeCategory = null;
    public ListView<Expense> expensesListView;
    public Label expensesCategoryLabel;
    public Category lastExpenseCategory = null;


    public void performViewIncomes(ActionEvent actionEvent) {
        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedCategoryItem.getValue();

        if (!categorySupervisedByLoggedInUser(selectedCategory.getId())) {
            new Alert(Alert.AlertType.ERROR, "You can't perform the action because you aren't supervising the category").show();
            return;
        }

        selectedCategory = selectedCategoryItem.getValue();
        tabPane.getSelectionModel().select(incomesTab);
        incomesCategoryLabel.setText(selectedCategory.toString());

        refreshIncomesListView(selectedCategory);
        lastIncomeCategory = selectedCategory;
    }

    private void refreshIncomesListView(Category incomesCategory) {
        incomesListView.getItems().removeIf(a -> true);
        incomesListView.getItems().addAll(incomesCategory.getIncomes());
        incomesListView.refresh();
    }

    public void performViewExpenses(ActionEvent actionEvent) {
        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Category selectedCategory = selectedCategoryItem.getValue();

        if (!categorySupervisedByLoggedInUser(selectedCategory.getId())) {
            new Alert(Alert.AlertType.ERROR, "You can't perform the action because you aren't supervising the category").show();
            return;
        }


        selectedCategory = selectedCategoryItem.getValue();
        tabPane.getSelectionModel().select(expensesTab);
        expensesCategoryLabel.setText(selectedCategory.toString());

        refreshExpensesListView(selectedCategory);
        lastExpenseCategory = selectedCategory;
    }

    private void refreshExpensesListView(Category expensesCategory) {
        expensesListView.getItems().removeIf(a -> true);
        expensesListView.getItems().addAll(expensesCategory.getExpenses());
        expensesListView.refresh();
    }

    public void performAddIncome(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/IncomeForm.fxml")));
        Parent root = loader.load();

        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();

        if (selectedCategoryItem != null) {
            Category selectedCategory = selectedCategoryItem.getValue();

            IncomeForm incomeForm = loader.getController();
            incomeForm.initializeForCreate(entityManagerFactory, loggedInUser, selectedCategory, incomesListView);

            ControllerUtil.showNewStage(root);
        }
    }

    public void performEditIncome(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/IncomeForm.fxml")));
        Parent root = loader.load();

        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Income incomeToEdit = incomesListView.getSelectionModel().getSelectedItem();

        if (selectedCategoryItem != null && incomeToEdit != null) {
            Category selectedCategory = selectedCategoryItem.getValue();

            IncomeForm incomeForm = loader.getController();
            incomeForm.initializeForEdit(entityManagerFactory, loggedInUser, selectedCategory, incomeToEdit, incomesListView);

            ControllerUtil.showNewStage(root);
        }
    }

    public void performDeleteIncome(ActionEvent actionEvent) {
        Income incomeToDelete = incomesListView.getSelectionModel().getSelectedItem();

        if (incomeToDelete != null && lastIncomeCategory != null) {
            IncomeHibernateControl incomeHibernateControl = new IncomeHibernateControl(entityManagerFactory);
            incomeHibernateControl.remove(incomeToDelete.getId());
//            lastIncomeCategory.getIncomes().remove(incomeToDelete);
            incomesListView.getItems().remove(incomeToDelete);
            incomesListView.refresh();
        }
    }

    public void performAddExpense(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/ExpenseForm.fxml")));
        Parent root = loader.load();

        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();

        if (selectedCategoryItem != null) {
            Category selectedCategory = selectedCategoryItem.getValue();

            ExpenseForm expenseForm = loader.getController();
            expenseForm.initializeForCreate(entityManagerFactory, loggedInUser, selectedCategory, expensesListView);

            ControllerUtil.showNewStage(root);
        }
    }

    public void performEditExpense(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/ExpenseForm.fxml")));
        Parent root = loader.load();

        TreeItem<Category> selectedCategoryItem = categoriesTreeView.getSelectionModel().getSelectedItem();
        Expense expenseToEdit = expensesListView.getSelectionModel().getSelectedItem();

        if (selectedCategoryItem != null && expenseToEdit != null) {
            Category selectedCategory = selectedCategoryItem.getValue();

            ExpenseForm expenseForm = loader.getController();
            expenseForm.initializeForEdit(entityManagerFactory, loggedInUser, selectedCategory, expenseToEdit, expensesListView);

            ControllerUtil.showNewStage(root);
        }
    }

    public void performDeleteExpense(ActionEvent actionEvent) {
        Expense expenseToDelete = expensesListView.getSelectionModel().getSelectedItem();

        if (expenseToDelete != null && lastExpenseCategory != null) {
            ExpenseHibernateControl expenseHibernateControl = new ExpenseHibernateControl(entityManagerFactory);
            expenseHibernateControl.remove(expenseToDelete.getId());
            expensesListView.getItems().remove(expenseToDelete);
            expensesListView.refresh();
        }
    }

    public void performDeleteThisUser(ActionEvent actionEvent) {
        Alert warningAlert = new Alert(Alert.AlertType.WARNING, "Are you sure you want to delete your account?");
        Optional<ButtonType> buttonType = warningAlert.showAndWait();
        if (buttonType.get() == ButtonType.OK) {
            userHibernateControl.deleteUser(loggedInUser.getId());
            closeActionEventsStage(actionEvent);
        }
    }

    public void performEditAccountInfo(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/forms/AccountDataForm.fxml")));
        Parent root = loader.load();

        AccountDataForm accountDataForm = loader.getController();
        accountDataForm.customInitialize(entityManagerFactory, loggedInUser);

        ControllerUtil.showNewStage(root);
    }

    public void performSystemTotalBalance(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(("../view/AccSysBalanceWindow.fxml")));
        Parent root = loader.load();

        AccSysBalanceWindow accSysBalanceWindow = loader.getController();
        accSysBalanceWindow.customInitialize(entityManagerFactory);

        ControllerUtil.showNewStage(root);
    }

    public void performRefreshView(ActionEvent actionEvent) {
        refillCategoriesTreeView();
    }

    public boolean categorySupervisedByLoggedInUser(Long categoryId) {
        Category category = categoryHibernateControl.getCategoryById(categoryId);

        for (User supervisor : category.getSupervisors()) {
            if (supervisor.getId() == loggedInUser.getId()) {
                return true;
            }
        }
        return false;
    }

    public AccountingSystem getAccountingSystem() {
        return accountingSystem;
    }

    public void setAccountingSystem(AccountingSystem accountingSystem) {
        this.accountingSystem = accountingSystem;
    }

    public TreeView<Category> getCategoriesTreeView() {
        return categoriesTreeView;
    }

    public void setCategoriesTreeView(TreeView<Category> categoriesTreeView) {
        this.categoriesTreeView = categoriesTreeView;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }
}
