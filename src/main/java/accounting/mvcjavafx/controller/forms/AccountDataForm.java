package accounting.mvcjavafx.controller.forms;

import accounting.mvcjavafx.controller.ControllerUtil;
import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.Company;
import accounting.mvcjavafx.model.Person;
import accounting.mvcjavafx.model.User;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import javax.persistence.EntityManagerFactory;

public class AccountDataForm {
    public TextField usernameField;
    public PasswordField passwordField;
    public TextField phoneNumberField;
    public TextField emailField;
    public TextField addressField;
    public TextField firstNameField;
    public TextField lastNameField;
    public TextField companyNameField;

    private EntityManagerFactory entityManagerFactory;
    private UserHibernateControl userHibernateControl;
    private User loggedInUser;

    public void customInitialize(EntityManagerFactory entityManagerFactory, User loggedInUser) {
        this.entityManagerFactory = entityManagerFactory;
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
        this.loggedInUser = userHibernateControl.getUserById(loggedInUser.getId());

        usernameField.setText(loggedInUser.getUsername());
        passwordField.setText(loggedInUser.getPassword());
        phoneNumberField.setText(loggedInUser.getContactInfo().getPhoneNumber());
        emailField.setText(loggedInUser.getContactInfo().getEmail());
        addressField.setText(loggedInUser.getContactInfo().getAddress());

        if (loggedInUser instanceof Person) {
            firstNameField.setText(((Person) loggedInUser).getFirstName());
            lastNameField.setText(((Person) loggedInUser).getLastName());
            companyNameField.editableProperty().set(false);
            companyNameField.setPromptText("Not applicable");
        } else if (loggedInUser instanceof Company) {
            companyNameField.setText(((Company) loggedInUser).getCompanyName());
            firstNameField.editableProperty().set(false);
            lastNameField.editableProperty().set(false);
            firstNameField.setPromptText("Not applicable");
            lastNameField.setPromptText("Not applicable");
        }
    }


    public void performConfirm(ActionEvent actionEvent) {
        if (usernameField.getText().isEmpty()) {
            new Alert(Alert.AlertType.ERROR, "Username can't be empty").show();
            return;
        }
        if (!loggedInUser.getUsername().equals(usernameField.getText()) &&
                userHibernateControl.isUsernameTaken(usernameField.getText())) {
            new Alert(Alert.AlertType.ERROR, "The username is already taken").show();
            return;
        }
        if (passwordField.getText().isEmpty()) {
            new Alert(Alert.AlertType.ERROR, "Password can't be empty").show();
            return;
        }


        loggedInUser.setUsername(usernameField.getText());
        loggedInUser.setPassword(passwordField.getText());
        loggedInUser.getContactInfo().setPhoneNumber(phoneNumberField.getText());
        loggedInUser.getContactInfo().setEmail(emailField.getText());
        loggedInUser.getContactInfo().setAddress(addressField.getText());

        if (loggedInUser instanceof Person) {
            ((Person) loggedInUser).setFirstName(firstNameField.getText());
            ((Person) loggedInUser).setLastName(lastNameField.getText());
            userHibernateControl.edit(loggedInUser);
        } else if (loggedInUser instanceof Company) {
            ((Company) loggedInUser).setCompanyName(companyNameField.getText());
            userHibernateControl.edit(loggedInUser);
        }

        ControllerUtil.closeActionEventsStage(actionEvent);
    }
}
