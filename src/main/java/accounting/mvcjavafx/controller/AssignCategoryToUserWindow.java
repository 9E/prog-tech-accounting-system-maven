package accounting.mvcjavafx.controller;

import accounting.mvcjavafx.hibernate.UserHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;

import javax.persistence.EntityManagerFactory;

import static accounting.mvcjavafx.controller.ControllerUtil.closeActionEventsStage;

public class AssignCategoryToUserWindow {
    public TextField usernameField;

    private UserHibernateControl userHibernateControl;
    private User loggedInUser;
    private TreeItem<Category> categoryTreeItem;

    public void customInitialize(EntityManagerFactory entityManagerFactory, User loggedInUser, TreeItem<Category> categoryTreeItem) {
        this.loggedInUser = loggedInUser;
        this.categoryTreeItem = categoryTreeItem;
        this.userHibernateControl = new UserHibernateControl(entityManagerFactory);
    }

    public void performConfirm(ActionEvent actionEvent) {
        String username = usernameField.getText();
        if (userHibernateControl.isUsernameTaken(username)) {
            User user = userHibernateControl.getUserByUsername(username);
            loggedInUser.assignCategoryTo(user, categoryTreeItem.getValue());
            userHibernateControl.edit(user);

            closeActionEventsStage(actionEvent);
        } else {
            new Alert(Alert.AlertType.ERROR, "Such user doesn't exist").show();
        }
    }
}
