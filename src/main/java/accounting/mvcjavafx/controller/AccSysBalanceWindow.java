package accounting.mvcjavafx.controller;

import accounting.mvcjavafx.hibernate.ExpenseHibernateControl;
import accounting.mvcjavafx.hibernate.IncomeHibernateControl;
import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import javafx.event.ActionEvent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import javax.persistence.EntityManagerFactory;
import java.time.LocalDate;
import java.util.List;

public class AccSysBalanceWindow {
    public DatePicker fromDatePicker;
    public DatePicker toDatePicker;
    public TextField totalIncomeField;
    public TextField totalExpensesField;
    public TextField totalBalanceField;

    private EntityManagerFactory entityManagerFactory;
    private IncomeHibernateControl incomeHibernateControl;
    private ExpenseHibernateControl expenseHibernateControl;

    public void customInitialize(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
        this.incomeHibernateControl = new IncomeHibernateControl((entityManagerFactory));
        this.expenseHibernateControl = new ExpenseHibernateControl((entityManagerFactory));
    }

    public void performGetResults(ActionEvent actionEvent) {
        LocalDate fromDate = fromDatePicker.getValue();
        LocalDate toDate = toDatePicker.getValue();

        double totalIncome = incomeHibernateControl.getTotalIncomeByDate(fromDate, toDate);
        double totalExpenses = expenseHibernateControl.getTotalExpensesByDate(fromDate, toDate);

        totalIncomeField.setText("" + totalIncome);
        totalExpensesField.setText("" + totalExpenses);
        totalBalanceField.setText("" + (totalIncome - totalExpenses));
    }
}
