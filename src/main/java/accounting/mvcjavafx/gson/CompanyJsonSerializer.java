package accounting.mvcjavafx.gson;

import accounting.mvcjavafx.model.Company;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class CompanyJsonSerializer implements JsonSerializer<Company> {
    @Override
    public JsonElement serialize(Company company, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", company.getId());
        jsonObject.addProperty("username", company.getUsername());
        jsonObject.addProperty("password", company.getPassword());
        jsonObject.add("contactInfo", jsonSerializationContext.serialize(company.getContactInfo()));
        jsonObject.add("supervisedCategories", jsonSerializationContext.serialize(company.getSupervisedCategories()));
        jsonObject.addProperty("creationDate", company.getCreationDate().toString());
        jsonObject.addProperty("companyName", company.getCompanyName());
        return jsonObject;
    }
}
