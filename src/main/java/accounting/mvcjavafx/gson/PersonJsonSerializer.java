package accounting.mvcjavafx.gson;

import accounting.mvcjavafx.model.Person;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class PersonJsonSerializer implements JsonSerializer<Person> {
    @Override
    public JsonElement serialize(Person person, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", person.getId());
        jsonObject.addProperty("username", person.getUsername());
        jsonObject.addProperty("password", person.getPassword());
        jsonObject.add("contactInfo", jsonSerializationContext.serialize(person.getContactInfo()));
        jsonObject.add("supervisedCategories", jsonSerializationContext.serialize(person.getSupervisedCategories()));
        jsonObject.addProperty("creationDate", person.getCreationDate().toString());
        jsonObject.addProperty("firstName", person.getFirstName().toString());
        jsonObject.addProperty("lastName", person.getLastName().toString());
        return jsonObject;
    }
}
