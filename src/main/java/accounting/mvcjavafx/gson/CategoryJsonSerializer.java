package accounting.mvcjavafx.gson;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.*;

import java.lang.reflect.Type;

public class CategoryJsonSerializer implements JsonSerializer<Category> {
    @Override
    public JsonElement serialize(Category category, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("id", category.getId());
        jsonObject.addProperty("name", category.getName());
        jsonObject.add("incomes", jsonSerializationContext.serialize(category.getIncomes()));
        jsonObject.add("expenses", jsonSerializationContext.serialize(category.getExpenses()));
        jsonObject.addProperty("creationDate", category.getCreationDate().toString());
        jsonObject.add("subcategories", jsonSerializationContext.serialize(category.getSubcategories()));

        return jsonObject;
    }
}
