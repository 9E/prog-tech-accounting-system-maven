package accounting.mvcjavafx.gson;

import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

public class ExpenseJsonSerializer implements JsonSerializer<Expense> {

    @Override
    public JsonElement serialize(Expense expense, Type type, JsonSerializationContext jsonSerializationContext) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", expense.getId());
        jsonObject.addProperty("name", expense.getName());
        jsonObject.addProperty("amount", expense.getAmount());
        jsonObject.addProperty("creationDate", expense.getCreationDate().toString());
        jsonObject.addProperty("receiptNumber", expense.getReceiptNumber());
        return jsonObject;
    }
}
