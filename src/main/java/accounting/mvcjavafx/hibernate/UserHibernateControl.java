package accounting.mvcjavafx.hibernate;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class UserHibernateControl extends HibernateControl{

    public UserHibernateControl(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, User.class);
    }

    public void deleteUser(Long id) {
        EntityManager entityManager = null;
        try{
            entityManager = createEntityManager();
            entityManager.getTransaction().begin();
            User userToRemove = null;

            try{
                userToRemove = entityManager.getReference(User.class, id);
                userToRemove.getId();

                List<Category> supervisedCategories = userToRemove.getSupervisedCategories();
                if (supervisedCategories != null && supervisedCategories.size() > 0) {
                    CategoryHibernateControl categoryHibernateControl = new CategoryHibernateControl(getEntityManagerFactory());

                    for (Category category : supervisedCategories) {
                        categoryHibernateControl.remove(category.getId());
                    }
                    entityManager.merge(userToRemove);
                }

                if (userToRemove.getAccountingSystem() != null) {
                    AccountingSystem accountingSystem = userToRemove.getAccountingSystem();
                    accountingSystem.getSystemUsers().remove(userToRemove);
                    entityManager.merge(accountingSystem);
                }

//                entityManager.merge(userToRemove);

            }catch (Exception e) {
                System.out.println("None found");
            }

            if (userToRemove != null)
                entityManager.remove(userToRemove);

            entityManager.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(entityManager != null){
                entityManager.close();
            }
        }
    }

    public User getUserByUsername(String username) {
        for (User user : getAllUsers()) {
            if (username.equals(user.getUsername())) {
                return user;
            }
        }
        return null;
    }

    public boolean isUsernameTaken(String username) {
        for (User user : getAllUsers()) {
            if (username.equals(user.getUsername())) {
                return true;
            }
        }
        return false;
    }

    public boolean isLoginInfoCorrect(String username, String password) {
        for (User user : getAllUsers()) {
            if (doCredentialsMatchWithUserAccount(username, password, user)) {
                return true;
            }
        }
        return false;
    }

    private boolean doCredentialsMatchWithUserAccount(String username, String password, User user) {
        return username.equals(user.getUsername()) &&
                password.equals(user.getPassword());
    }
    
    public void create(Object object) {
        executeInTransaction(() -> entityManager.persist(object));
    }

    public void edit(Object object) {
        executeInTransaction(() -> entityManager.merge(object));
    }

    public <T> void remove(T id) {
        executeInTransaction(() -> {
            Object objectToRemove = entityManager.find(getObjectClass(), id);

            if (objectToRemove != null) {
                entityManager.remove(objectToRemove);
            }
        });
    }

    public <T> User getUserById(T id) {
        return (User) getObjectById(id);
    }

    public List<User> getAllUsers() {
        return getAllObjects();
    }
}
