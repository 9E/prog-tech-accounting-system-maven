package accounting.mvcjavafx.hibernate;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.accsys.AccountingSystem;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class CategoryHibernateControl extends HibernateControl {

    public CategoryHibernateControl(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, Category.class);
    }

    public void create(Object object) {
        executeInTransaction(() -> entityManager.persist(object));
    }

    public void edit(Object category) {
        executeInTransaction(() -> entityManager.merge(category));
    }

    public void remove(Long id) {
        EntityManager entityManager = null;
        try{
            entityManager = createEntityManager();
            entityManager.getTransaction().begin();
            Category categoryToRemove = null;

            try{
                categoryToRemove = entityManager.getReference(Category.class, id);
                categoryToRemove.getId();

                if (categoryToRemove.hasParentCategory()) {
                    Category parentCategory = categoryToRemove.getParentCategory();
                    parentCategory.getSubcategories().remove(categoryToRemove);
                    entityManager.merge(parentCategory);
                }

                User creator = categoryToRemove.getCreator();
                creator.getCreatedCategories().remove(categoryToRemove);
                entityManager.merge(creator);

                AccountingSystem accountingSystem = categoryToRemove.getAccountingSystem();
                accountingSystem.getSystemRootCategories().remove(categoryToRemove);
                entityManager.merge(accountingSystem);

                for (User user : categoryToRemove.getSupervisors()) {
                    user.getSupervisedCategories().remove(categoryToRemove);
                    entityManager.merge(user);
                }

                if (categoryToRemove.getSubcategories().size() != 0) {
                    for (Category category : categoryToRemove.getSubcategories()) {
                        remove(category.getId());
                    }
                }
                categoryToRemove.getSupervisors().clear();
                categoryToRemove.getSubcategories().clear();
                entityManager.merge(categoryToRemove);

            }catch (Exception e) {
                System.out.println("None found");
            }

            if (categoryToRemove != null)
                entityManager.remove(categoryToRemove);

            entityManager.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(entityManager != null){
                entityManager.close();
            }
        }
    }

    public <T> Category getCategoryById(T id) {
        return (Category) getObjectById(id);
    }

    public List<Category> getAllRootCategories() {
        List<Category> categories = getAllCategories();
        categories.removeIf(Category::hasParentCategory);
        return categories;
    }

    public List<Category> getAllSupervisedCategories(Long userId) {
        List<Category> categories = getAllObjects();
        categories.removeIf(category -> !category.isSupervisedBy(userId));
        return categories;
    }

    public List<Category> getAllCategories() {
        return getAllObjects();
    }
}
