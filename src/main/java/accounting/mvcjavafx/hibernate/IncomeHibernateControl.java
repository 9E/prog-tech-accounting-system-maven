package accounting.mvcjavafx.hibernate;

import accounting.mvcjavafx.model.Category;
import accounting.mvcjavafx.model.User;
import accounting.mvcjavafx.model.moneyflow.Expense;
import accounting.mvcjavafx.model.moneyflow.Income;

import javax.persistence.EntityManagerFactory;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class IncomeHibernateControl extends HibernateControl {

    public IncomeHibernateControl(EntityManagerFactory entityManagerFactory) {
        super(entityManagerFactory, Income.class);
    }

    public void remove(Long id) {
        executeInTransaction(() -> {
            Income income = (Income) entityManager.find(getObjectClass(), id);
            Category owningCategory = income.getCategory();
            owningCategory.getIncomes().remove(income);
        });
    }

    public Income getIncomeById(Long id) {
        return (Income) getObjectById(id);
    }

    public List<Income> getAllIncomesOfCategory(Long categoryId) {
        List<Income> incomes = getAllIncomes();
        incomes.removeIf(income -> !Objects.equals(income.getCategory().getId(), categoryId));
        return incomes;
    }

    public List<Income> getAllIncomes() {
        return getAllObjects();
    }

    public void createIncome(Income income, Long incomeCategoryId) {
        executeInTransaction(() -> {
            Category category = entityManager.find(Category.class, incomeCategoryId);
            User user = entityManager.find(category.getCreator().getClass(), category.getCreator().getId());
            income.setCreator(user);
            income.setCategory(category);
            category.addIncome(income);
        });
    }

    public void editIncome(Income incomeToEdit) {
        executeInTransaction(() -> {
            Income income = entityManager.find(incomeToEdit.getClass(), incomeToEdit.getId());
            income.setAmount(incomeToEdit.getAmount());
            income.setName(incomeToEdit.getName());
        });
    }

    public double getTotalIncomeByDate(LocalDate fromDate, LocalDate toDate) {
        LocalDate comparedFromDate = fromDate == null ? LocalDate.MIN : fromDate;
        LocalDate comparedToDate = toDate == null ? LocalDate.MAX : toDate;

        System.out.println(fromDate);
        System.out.println(toDate);

        List<Income> incomes = getAllIncomes();

            incomes.removeIf(income -> income.getCreationDate().isBefore(comparedFromDate) ||
                    income.getCreationDate().isAfter(comparedToDate));

        Category tempCategory = new Category();
        tempCategory.setIncomes(incomes);

        return tempCategory.getSumOfIncomesWithSubcategories();
    }
}
