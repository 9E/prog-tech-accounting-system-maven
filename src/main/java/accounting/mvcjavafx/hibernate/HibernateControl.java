package accounting.mvcjavafx.hibernate;

import accounting.mvcjavafx.model.Category;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public abstract class HibernateControl {

    private EntityManagerFactory entityManagerFactory;
    protected EntityManager entityManager;
    private Class objectClass;

    public HibernateControl(EntityManagerFactory entityManagerFactory, Class objectClass) {
        this.entityManagerFactory = entityManagerFactory;
        this.entityManager = null;
        this.objectClass = objectClass;
    }

    public void create(Object object) {
        executeInTransaction(() -> entityManager.persist(object));
    }

    public void edit(Object category) {
        executeInTransaction(() -> entityManager.merge(category));
    }


    protected void executeInTransaction(Runnable lambda) {
        try {
            entityManager = createEntityManager();
            entityManager.getTransaction().begin();

            lambda.run();

            entityManager.getTransaction().commit();
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
    }

    public EntityManager createEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    public Class getObjectClass() {
        return objectClass;
    }

    public void setObjectClass(Class objectClass) {
        this.objectClass = objectClass;
    }

    public <T> Object getObjectById(T id) {
        entityManager = createEntityManager();
        Object object = entityManager.find(getObjectClass(), id);
        entityManager.close();

        return object;
    }

    public List getAllObjects() {
        EntityManager entityManager = null;

        try {
            entityManager = createEntityManager();

            CriteriaQuery criteriaQuery = entityManager.getCriteriaBuilder().createQuery();
            criteriaQuery.select(criteriaQuery.from(getObjectClass()));
            Query query = entityManager.createQuery(criteriaQuery);

            return query.getResultList();
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
        return null;
    }

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }
}
